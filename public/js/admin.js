/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./dist/js/admin.js":
/*!**************************!*\
  !*** ./dist/js/admin.js ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

jQuery(function ($) {
  console.log('test'); // resize();
  // function resize() {
  //   setTimeout(function () {
  //       // Main container
  //       var max = $('.mce-tinymce')
  //             .css('border', 'none')
  //             .parent().outerHeight();
  //       // Menubar
  //       max += -$('.mce-menubar.mce-toolbar').outerHeight(); 
  //       // Toolbar
  //       max -= $('.mce-toolbar-grp').outerHeight(); 
  //       // Random fix lawl - why 1px? no one knows
  //       max -= 1;
  //       console.log('test')
  //       // Set the new height
  //       $('.mce-container').height(50);
  //   }, 200); 
  //   console.log('function resize')
  // $('.submitdelete').each(function(e) {
  //   $(this).on('click', function(e) {      
  //       if( ! confirm("You really want to delete the post?") ) {
  //           e.preventDefault();
  //           return;
  //       }          
  //   })
  // })
  // $( '.upload_img' ).on( 'click', function(e) {
  //     // console.log(this)
  //     var upload_field = $(this).next()
  //     var replace_url = $(this).prev()
  //     let send_attachment_bkp = wp.media.editor.send.attachment;
  //     wp.media.editor.send.attachment = function (props, attachment) {
  //         console.log(upload_field);
  //         upload_field.val(attachment.id);
  //         replace_url.attr('src', attachment.url);
  //         wp.media.editor.send.attachment = send_attachment_bkp;
  //     };
  //     wp.media.editor.open();
  //     return false;
  // });
  // $( '.remove_img' ).on( 'click', function(e) {
  //     $(this).prev().val('');
  //     $('.preview_img').attr('src', '');
  //     return false;
  // });
  // var container = $('.sortable-container');
  // setIndex()
  // container.delegate('.wis-box', 'mouseenter mouseout', handleMouse);
  // container.sortable({
  //   update: function () {
  //     setIndex()
  //   }
  // })
  // $(document).on('click', 'button.wis', function (e) {
  //   e.preventDefault()
  //   var btn = $(this)
  //   var val = btn.val()
  //   var item = btn.closest('.wis-box')
  //   if (val == 'up')
  //     moveUp(item)
  //   else if (val == 'down')
  //     moveDown(item)
  //   else if (val == 'create')
  //     create(item)
  //   else if (val == 'addImage')
  //     addImage($(this).data('file'))
  //   else if (val == 'removeImage')
  //     removeImage($(this).data('file'))  
  //   else
  //     remove(item)
  // })
  // function handleMouse(e) {
  //   if (e.type == "mouseenter") {
  //     $(this).addClass('highlight');
  //   } else if (e.type == "mouseout") {
  //     $(this).removeClass('highlight');
  //   }
  // }
  // function setIndex() {
  //   $('.wis-admin-item').each(function () {
  //     var index = $(this).closest('.wis-box').index()
  //     var index = index + 1
  //     if($(this).hasClass('wp-editor-area')) {
  //       var name = this.classList[1];
  //       var value = this.value
  //     } else {
  //       var name = $(this).attr('data-id')
  //     }
  //     $(this).not('.btn').attr('id', name + '_' + index)
  //     $(this).not('.btn').attr('name', name + '_' + index)
  //     $(this).filter('.btn').attr('data-file', 'image_' + index)
  //     $(this).filter('.count').html(index)
  //   })
  //   console.log('setIndex');
  //   var box_count = $('.wis-box').length
  //   $('#count').attr('value', box_count)
  // }
  // function moveUp(item) {
  //   var prev = item.prev();
  //   if (prev.length == 0)
  //     return
  //   prev.css('z-index', 999).css('position', 'relative').animate({ top: item.height() }, 250);
  //   item.css('z-index', 1000).css('position', 'relative').animate({ top: '-' + prev.height() }, 300, function () {
  //     prev.css('z-index', '').css('top', '').css('position', '');
  //     item.css('z-index', '').css('top', '').css('position', '');
  //     item.insertBefore(prev);
  //     setIndex()
  //   });
  // }
  // function moveDown(item) {
  //   var next = item.next();
  //   if (next.length == 0)
  //     return
  //   next.css('z-index', 999).css('position', 'relative').animate({ top: '-' + item.height() }, 250);
  //   item.css('z-index', 1000).css('position', 'relative').animate({ top: next.height() }, 300, function () {
  //     next.css('z-index', '').css('top', '').css('position', '');
  //     item.css('z-index', '').css('top', '').css('position', '');
  //     item.insertAfter(next);
  //     setIndex()
  //   });
  // }
  // function remove(item) {
  //   if (confirm('Möchtest du die Box wirklich löschen?')) {
  //     item.remove()
  //     setIndex()
  //   }
  //   return false
  // }
  // function create(item) {
  //   var newItem = item.clone().find('input:text').val('').end()
  //   newItem = newItem.find('input:hidden').val('').end()
  //   newItem = newItem.find('img').removeAttr('src').end()
  //   newItem = newItem.find('textarea').val('').end()
  //   newItem.insertAfter(item)
  //   setIndex()
  // }
  // function addImage(item) {
  //     console.log(item);
  //   wp.media.editor.send.attachment = function (props, attachment) {
  //     $('#' + item).attr('src', attachment.url)
  //     $('#file_' + item).attr('value', attachment.id)
  //   }
  //   wp.media.editor.open();
  // }
  // function removeImage(item) {
  //   $('#' + item).attr('src', '')
  //   $('#file_' + item).attr('value', '')
  // }
});

/***/ }),

/***/ 1:
/*!********************************!*\
  !*** multi ./dist/js/admin.js ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/sam/Projects/docker-projects/masken/wp_data/wp-content/themes/visual-composer-starter-child/dist/js/admin.js */"./dist/js/admin.js");


/***/ })

/******/ });