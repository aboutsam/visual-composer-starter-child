/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./dist/js/app.js":
/*!************************!*\
  !*** ./dist/js/app.js ***!
  \************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'owl.carousel'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
// const { render } = require("sass");


(function ($) {
  $('#product-info a').click(function () {
    var prodId = $(this).data('link');
    $('html,body').animate({
      scrollTop: $('#' + prodId).offset().top - $(window).height() / 3
    }, 300);
    console.log('test');
  });
  $('.btn-cart').click(function (e) {
    console.log('button cart'); //e.stopPropagation()

    $('#cart-menu').toggleClass('open');
    e.stopPropagation(); // setOverlay()
  });
  $('#search').click(function (e) {
    e.stopPropagation();
    console.log('search');
  });
  $('.btn-burger').click(function (e) {
    e.stopPropagation();
    $('#main-menu').toggleClass('open');
    setOverlay();
  });
  $(window).load(function () {
    addTopHeightToFixedElements();
  });
  $(window).resize(function () {
    addTopHeightToFixedElements();
  }); // add top px to fixed element to manage wp-admin bar height

  function addTopHeightToFixedElements() {
    if ($('#wpadminbar').length !== 0) {
      var height = $('#wpadminbar').height();
      $('.fixed').css('top', height);
    }
  }

  function setOverlay() {
    $('.wis-overlay').removeClass('show');

    if ($('#cart-menu').hasClass('open') || $('#main-menu').hasClass('open')) {
      $('.wis-overlay').addClass('show');
    }
  }

  $('body').click(function () {
    $('.wis-overlay').removeClass('show');
    $('#cart-menu').removeClass('open');
    $('#main-menu').removeClass('open');
    console.log('huhu');
  });
  $(document.body).trigger('wc_fragment_refresh');
  $('.wis-block').each(function () {
    var blockname = $(this).data('blockname');
    var blockSettings = $(this).data('block-settings');
    console.log(blockname);
    console.log(blockSettings); // $(this).addClass('show-settings')
  });
  $(document).ready(function () {
    $('.owl-carousel.start').owlCarousel({
      center: true,
      items: 1,
      loop: true,
      margin: 10,
      nav: true,
      autoplay: true,
      autoplayTimeout: 5000,
      autoplayHoverPause: true,
      navClass: ['md:-ml-12', 'md:-mr-12'],
      navContainerClass: 'flex justify-between py-4 md:absolute top-1/2 w-full',
      navText: ['<img src="https://curyu.com/wp-content/uploads/2020/12/curyu-arrow.png" class="transform rotate-180">', '<img src="https://curyu.com/wp-content/uploads/2020/12/curyu-arrow.png" />']
    });
  });
  $(document).ready(function () {
    $('.owl-carousel.product').owlCarousel({
      center: true,
      items: 1,
      loop: true,
      margin: 10,
      nav: true,
      navContainerClass: 'flex justify-between py-4 hidden',
      navClass: ['owl-prev', 'owl-next'],
      navText: ['<img src="https://curyu.com/wp-content/uploads/2020/12/curyu-arrow.png" class="transform rotate-180">', '<img src="https://curyu.com/wp-content/uploads/2020/12/curyu-arrow.png" />']
    });
  }); // console.log('app.js')
})(jQuery);

/***/ }),

/***/ 0:
/*!**************************************************!*\
  !*** multi ./dist/js/app.js ./dist/css/app.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /Users/sam/Projects/docker-projects/masken/wp_data/wp-content/themes/visual-composer-starter-child/dist/js/app.js */"./dist/js/app.js");
!(function webpackMissingModule() { var e = new Error("Cannot find module '/Users/sam/Projects/docker-projects/masken/wp_data/wp-content/themes/visual-composer-starter-child/dist/css/app.scss'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());


/***/ })

/******/ });