const images = document.querySelectorAll("[data-src]")

const imgOptions = {
    threshold: 1,
}

const imgObserver = new IntersectionObserver((entries, imgObserver) => {
    entries.forEach(entry => {
        if (!entry.isIntersecting) {
            return
        } else {
            preloadImage(entry.target)
            imgObserver.unobserve(entry.target)
        }
    })
}, imgOptions)

function preloadImage(img) {
    //console.log('in screen')
    //console.log('preloadImages')
    const src = img.getAttribute("data-src")
    if (!src) {
        return
    }
    //img.closest('div.grid-item').classList.remove('placeholder')
    //console.log(img.closest('div.grid-item'))
    img.src = src;
}

images.forEach(image => {
    imgObserver.observe(image)
})