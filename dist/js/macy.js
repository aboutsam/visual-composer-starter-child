import macy from 'macy/dist/macy.js'

var macyGallery = document.querySelector('.macy-gallery');
var macySingleGallery = document.querySelector('.macy-single-gallery');


// toDo: write better function
if ( macyGallery !== null) {
  var macyInstance = macy({
    container: '#macy-container',
    trueOrder: false,
    waitForImages: true,
    useImageLoader: true,
    margin: 20,
    columns: 3,
    breakAt: {
        990: {
          columns: 2
        },
        600: {
          columns: 1,
          margin: 0,
        }
    }
  });

  macyInstance.recalculate();

  macyInstance.runOnImageLoad(function () {
    var macyGallery = document.querySelector('#event-grid');
    macyGallery.classList.add('appear');
    macyInstance.recalculate(true, true);
  });
}

if ( macySingleGallery !== null) {
  macy({
    container: '#single-event',
    trueOrder: false,
    waitForImages: false,
    margin: 20,
    columns: 2,
    breakAt: {
        600: {
          columns: 1,
          margin: 0,
        }
    }
  });
}