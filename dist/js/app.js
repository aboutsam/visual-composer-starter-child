// const { render } = require("sass");

import 'owl.carousel';

(function ($) {

  $('#product-info a').click(function() {
    var prodId = $(this).data('link')
    $('html,body').animate({ scrollTop: $('#'+prodId).offset().top - $(window).height()/3 }, 300);
    console.log('test')
  })

  $('.btn-cart').click(function(e) {
    console.log('button cart')
    //e.stopPropagation()
    $('#cart-menu').toggleClass('open')
    e.stopPropagation()
    // setOverlay()
  })

  $('#search').click(function(e) {
    e.stopPropagation()
    console.log('search')
  })

  $('.btn-burger').click(function(e) {
    e.stopPropagation()
    $('#main-menu').toggleClass('open')
    setOverlay()
  })

  $(window).load(function() {
    addTopHeightToFixedElements()
  })

  $( window ).resize(function() {
    addTopHeightToFixedElements()
  })

  // add top px to fixed element to manage wp-admin bar height
  function addTopHeightToFixedElements() {
    if( $('#wpadminbar').length !== 0 ) {
      var height = $('#wpadminbar').height()
      $('.fixed').css('top', height)
    }
  }

  function setOverlay() {
    $('.wis-overlay').removeClass('show');
    if( $('#cart-menu').hasClass('open') || $('#main-menu').hasClass('open')) {
      $('.wis-overlay').addClass('show');
    }
  }

  $('body').click(function() {
    $('.wis-overlay').removeClass('show')
    $('#cart-menu').removeClass('open')
    $('#main-menu').removeClass('open')
    console.log('huhu')
  })

  $(document.body).trigger('wc_fragment_refresh');

  $('.wis-block').each(function(){
      var blockname = $(this).data('blockname')
      var blockSettings = $(this).data('block-settings')
      console.log(blockname)
      console.log(blockSettings)

      // $(this).addClass('show-settings')
  });

  $(document).ready(function(){
    $('.owl-carousel.start').owlCarousel({
      center: true,
      items:1,
      loop:true,
      margin:10,
      nav: true,
      autoplay:true,
      autoplayTimeout: 5000,
      autoplayHoverPause:true,
      navClass: ['md:-ml-12', 'md:-mr-12'],
      navContainerClass: 'flex justify-between py-4 md:absolute top-1/2 w-full',
      navText: ['<img src="https://curyu.com/wp-content/uploads/2020/12/curyu-arrow.png" class="transform rotate-180">','<img src="https://curyu.com/wp-content/uploads/2020/12/curyu-arrow.png" />']
    })
  });

  $(document).ready(function(){
    $('.owl-carousel.product').owlCarousel({
      center: true,
      items:1,
      loop:true,
      margin:10,
      nav: true,
      navContainerClass: 'flex justify-between py-4 hidden',
      navClass: ['owl-prev', 'owl-next'],
      navText: ['<img src="https://curyu.com/wp-content/uploads/2020/12/curyu-arrow.png" class="transform rotate-180">','<img src="https://curyu.com/wp-content/uploads/2020/12/curyu-arrow.png" />']
    })
  });

// console.log('app.js')

})(jQuery);

