const effects = document.querySelectorAll("[data-effect]")

const imgOptions = {
    threshold: 1,
}

const effectObserver = new IntersectionObserver((entries, effectObserver) => {
    entries.forEach(entry => {
        if (!entry.isIntersecting) {
            return
        } else {
            startEffect(entry.target)
            effectObserver.unobserve(entry.target)
        }
    })
}, imgOptions)

function startEffect(element) {
    // console.log('preloadImages')
    const effect = element.getAttribute("data-effect")
    if (!effect) {
        return
    }

    // console.log(jQuery(element).text())
    const durationTime = element.getAttribute("data-duration")
    // console.log(durationTime)

    jQuery(element).prop('Counter', 0)
      .animate({ Counter: jQuery(element).text() }, {
        duration: Number(durationTime),
        easing: 'swing',
        start: function() {
            
            element.classList.remove('opacity-0');
        },
        step: function (now) {    
          jQuery(element).text(Math.ceil(now)).remove('opacity-0');
        },
        complete: function() {
            element.classList.remove('w-150');
            element.classList.remove('w-73');
        }
    });

    // console.log('start effect');
}

effects.forEach(effect => {
    effectObserver.observe(effect)
})

const macyEffects = document.querySelectorAll("[data-macy-complete]")

macyEffects.forEach(effect => {
    macyObserver.observe(effect)
})

const macyObserver = new IntersectionObserver((entries, macyObserver) => {
    entries.forEach(entry => {
        if (!entry.isIntersecting) {
            return
        } else {
            const effect = element.getAttribute("data-macy-complete")
            console.log(effect)
            macyObserver.unobserve(entry.target)
        }
    })
})