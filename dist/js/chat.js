
  $('#chatBubbleToggle').toggle(
    function () {
      loadQuestion( 'Hallo! Kann ich dir helfen?', 3000 )
      $('#chatbotWindow').addClass('open');
      console.log('open')
    }, function () {
      $('#chatbotWindow').removeClass('open');
      console.log('close')
    }
  );

  $('#chatBtnClose').on('click', function(e) {
    e.preventDefault()
    $('#chatbotWindow').removeClass('open');
  })

  function loadQuestion( msg, timeout ) {
    console.log('loadQuestion')
    setTimeout(function(){
      addChatMessage( msg, false )
    }, 3000);    
  }

  $('#submitMsg').on('click', function() {
    var chatMsg = $('#chatMessage').val()
    console.log('submitMsg: ' + chatMsg)
    addChatMessage( chatMsg, true )
  })

  function addChatMessage( msg, customer ) {
    var customer = customer ? 'flex-row-reverse' : '';
    var chat = '<div class="chat-text">'
    chat += '<div class="flex '+ customer +'">'
    chat += '  <div class="ci"><div class="bubble">CB</div></div>'
    chat += '    <p class="message">'+ msg +'</p>'
    chat += '</div>'
    chat += '</div>'
    $('#chat-messages').append(chat)
  }