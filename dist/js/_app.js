// const { render } = require("sass");

(function ($) {

  $('#product-info').click(function() {
    console.log('test')
  })

  $('.btn-cart').click(function(e) {
    e.stopPropagation()
    $('#cart-menu').toggleClass('open')
    setOverlay()
  })

  $('#search').click(function(e) {
    e.stopPropagation()
    console.log('search')
  })

  $('.btn-burger').click(function(e) {
    e.stopPropagation()
    $('#main-menu').toggleClass('open')
    setOverlay()
  })

  $(window).load(function() {
    addTopHeightToFixedElements()
  })

  $( window ).resize(function() {
    addTopHeightToFixedElements()
  })

  // add top px to fixed element to manage wp-admin bar height
  function addTopHeightToFixedElements() {
    if( $('#wpadminbar').length !== 0 ) {
      var height = $('#wpadminbar').height()
      $('.fixed').css('top', height)
    }
  }

  function setOverlay() {
    $('.wis-overlay').removeClass('show');
    if( $('#cart-menu').hasClass('open') || $('#main-menu').hasClass('open')) {
      $('.wis-overlay').addClass('show');
    }
  }

  $('body').click(function() {
    $('.wis-overlay').removeClass('show')
    $('#cart-menu').removeClass('open')
    $('#main-menu').removeClass('open')
    console.log('huhu')
  })

  $(document.body).trigger('wc_fragment_refresh');

  $('.wis-block').each(function(){
      var blockname = $(this).data('blockname')
      var blockSettings = $(this).data('block-settings')
      console.log(blockname)
      console.log(blockSettings)

      // $(this).addClass('show-settings')
  });

  // console.log('app.js')

})(jQuery);

