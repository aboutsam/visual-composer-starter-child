const mix = require('laravel-mix'); // this will be slightly different in v4
const tailwindcss = require('tailwindcss');
// const glob = require('glob-all');
const PurgecssPlugin = require('purgecss-webpack-plugin');

class TailwindExtractor {
    static extract(content) {
        return content.match(/[A-Za-z0-9-_:\/]+/g) || [];
    }
}

mix.sass('dist/css/style.scss', '')
    .options({
        processCssUrls: false,
        postCss: [tailwindcss('./tailwind.config.js')],
    })
    .disableNotifications()


