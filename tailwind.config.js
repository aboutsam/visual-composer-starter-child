module.exports = {
  theme: {
    extend: {
      gridTemplateRows: {
        '9': 'repeat(9, minmax(0, 1fr))',
      },
      height: {
        '128': '32rem',
        '60r': '60rem',
        'nav': '87px',
      },
      colors: {
        'sky': {
          100: '#f9fbff',
          200: '#e9f0fe',
          300: '#e9f0fe',
        },
        'white': '#FFFFFF',
        'black': '#000000',
        'products': '#F9F9F9',
        'grey': '#C4C4C4',
        'light-grey': '#f4f4f4',

        'oo': '#F1AB6E',

        'purple': '#7e4396',
        'orange': '#fe5900',
        'turkis': '#24a9b8',
        'blue': '#0068bf',
        'kaki': '#595d3a',

        'purple-light': '#e599ee',
        'orange-light': '#fde58f',
        'turkis-light': '#9dfaf1',
        'blue-light': '#a6d6fa',
        'kaki-light': '#9aaf6c',
      },
      screens: {
        '2xl': '1600px',
      },
      spacing: {
        '72': '18rem',
        '84': '21rem',
        '96': '24rem',
      },
      borderWidth: {
        '175r': '1.75rem'
      },
      maxWidth: theme => {
        return {
          'screen-2xl': theme('screens.2xl'),
        }
      },
    },
    screens: {
      sm: '640px',
      md: '768px',
      lg: '1024px',
      xl: '1280px',
    },
    fontFamily: {
      display: ['Inter', 'sans-serif'],
      body: ['Inter', 'sans-serif'],
      'brandon': ['Brandon'],
      'peter': ['Peter'],
      'henk-work': ['Henk Work'],
      'henk-work-bold': ['Henk Work Bold'],
      'montserratlight': ['montserratlight'],
      'dm': ['DM Serif Display', 'sans-serif']
    },
    borderWidth: {
      default: '1px',
      '0': '0',
      '2': '2px',
      '3': '3px',
      '4': '4px',
      '6': '6px',
      '8': '8px',
    },
    inset: {
      '0': 0,
      auto: 'auto', 
      '1/2': '50%',
    },
    container: {
      center: true
    }
  },
  variants: {},
  plugins: [
    function({ addUtilities }) {
      const newUtilities = {
        '.bg-gradient': {
          background: 'linear-gradient(90deg, #d53369 0%, #daae51 100%)',
        },
        '.bg-green-white': {
          background: 'linear-gradient(90deg, #F1AB6E 0%, #F1AB6E 49.99%, #fff 50%, #fff 100%)',
        },
        '.bg-gradient-product': {
          background: 'linear-gradient(90deg, #f9f9f9 0%, #f6f6f6 100%)',
        },
        '.bg-orange-white': {
          background: 'linear-gradient(90deg, #F1AB6E 0%, #F1AB6E 49%, #fff 50%, #fff 100%)',
        },
        '.text-gradient': {
          'background-image': 'linear-gradient(90deg, #d53369 0%, #daae51 100%)',
          '-webkit-background-clip': 'text',
          '-webkit-text-fill-color': 'transparent'
        },
        '.rotateY-180deg': {
          transform: 'rotateY(180deg)',
        },
        '.skew-15deg': {
          transform: 'skewY(-15deg)',
        },
        '.transition-all-05': {
          transition: 'all 0.5s ease-in-out',
        },
        '.center': {
          display: 'flex',
          'justify-content': 'center',
          'align-items': 'center'
        }
        // '.bg-50': {
        //   'position': 'absolute',
        //   'top': '0',
        //   'left': '0',
        //   'width': '50%',
        //   'height': '100%',
        //   'z-index': '-1',
        // }
      }
      addUtilities(newUtilities)
    },
    require('tailwindcss'),
    require('autoprefixer')
  ]
}
